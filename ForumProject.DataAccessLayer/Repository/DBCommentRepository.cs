﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;
using System.Data;

namespace ForumProject.DataAccessLayer.Repository
{
    public class DbCommentRepository : IRepository<BaseComment>
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DbCommentRepository()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }
        
         public void Save(BaseComment entity)
           {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("INSERT INTO [dbo].[BaseComment](m_titleBaseComment,m_contentBaseComment,m_id_article,m_id_user) VALUES (@title,@content,@id_article,@id_user)", sqlConnection);
                sqlcmd.Parameters.AddWithValue("@title", entity.TitleBaseComment);
                sqlcmd.Parameters.AddWithValue("@content", entity.ContentBaseComment);
                sqlcmd.Parameters.AddWithValue("@id_article", entity.Id);
                sqlcmd.Parameters.AddWithValue("@id_user", entity.User.Id);
                sqlcmd.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }

        public void Delete(int id)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("DELETE FROM [dbo].[BaseComment] WHERE m_id=@id", sqlConnection);
                sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;
                using (SqlDataReader reader = sqlcmd.ExecuteReader(CommandBehavior.CloseConnection))
                    sqlConnection.Close();
            }
        }

        public BaseComment FindById(int id)
        {
            var user = new BaseComment();
            return user;
        }

        public void Update(BaseComment entity)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[BaseComment] SET m_title = @title, m_contentArticle = @content Where m_id = @id",
                        sqlConnection);
                //sqlcmd.Parameters.Add("@id", System.Data.SqlDbType.Int)
                //    .Value = entity.Id;
                //sqlcmd.Parameters.Add("@title", System.Data.SqlDbType.NVarChar)
                //    .Value = entity.Title;
                //sqlcmd.Parameters.Add("@content", System.Data.SqlDbType.NVarChar)
                //    .Value = entity.ContentArticle;

                using (SqlDataReader reader = sqlcmd.ExecuteReader(CommandBehavior.CloseConnection))
                    sqlConnection.Close();
            }
        }

        public List<BaseComment> GetList()
        {
            var listAllUser = new List<BaseComment>();            
            return listAllUser;
        }

        public int GetCountComment()
        {
            int count = 0;

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand("SELECT COUNT(m_id) FROM [dbo].[BaseComment]", sqlConnection);              
                count = (int)sqlcmd.ExecuteScalar();             
                sqlConnection.Close();

            }

            return count;
        }

        public bool Generate
        {
            get
            {
                return GetCountComment() != 0;
            }
        }
    }
}

