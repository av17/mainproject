﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;

namespace ForumProject.DataAccessLayer.Repository
{
    public class DbAuthorRepository : DbUserRepository
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DbAuthorRepository()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }
          
        public bool Generate
        {
            get
            {
                return GetCountUser() != 0;
            }
        }

        public List<Author> GetList()
        {
            var listAllAuthors = new List<Author>();
            try
            {
                using (var sqlConnection = new SqlConnection(ConnectionString))
                {
                    sqlConnection.Open();
                    var sqlcmd = new SqlCommand("SELECT * FROM [dbo].[Author]", sqlConnection);
                    using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            var dtoAuthor = new AuthorDto
                            {
                                
                                NickName = reader[1].ToString()
                               
                            };
                            listAllAuthors.Add(_dtoAutoMapper.GetAuthor(dtoAuthor));
                        }
                    }
                    sqlConnection.Close();
                }

            }
            catch (InvalidOperationException ex)
            {
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;
                str += "\n" + "\n";
                str += "\n" + "Stack Trace :" + ex.StackTrace;

            }
            catch (SqlException ex)
            {
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;

            }
            catch (Exception ex)
            {
                string str;
                str = "Source:" + ex.Source;
                str += "\n" + "Message:" + ex.Message;

            }

            return listAllAuthors;
        }
        
    }

}

