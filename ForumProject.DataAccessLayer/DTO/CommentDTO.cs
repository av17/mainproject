﻿namespace ForumProject.DataAccessLayer.DTO
{
    public class CommentDto
    {
        public int Id { get; set; }
        public string TitleBaseComment { get; set; }
        public string ContentBaseComment { get; set; }
        public UserDto User { get; set; }
        public ArticleDto Article { get; set; }
    }
}