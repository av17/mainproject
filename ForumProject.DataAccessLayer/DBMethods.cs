﻿using ForumProject.DataAccessLayer.DTO;
using ForumProject.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumProject.DataAccessLayer
{
    public class DBMethods
    {
        const string ConsKeyDefaultCnnString = "conn";
        protected readonly DtoAutoMapper _dtoAutoMapper;

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConsKeyDefaultCnnString].ConnectionString;
            }
        }

        public DBMethods()
        {
            _dtoAutoMapper = new DtoAutoMapper();
        }

        public List<BaseComment> GetBaseCommentByArticle(int id_article)
        {
            var baseComment = new BaseComment();
            var userComment = new User();
            var listComment = new List<BaseComment>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var command = new SqlCommand(string.Format("SELECT m_id, m_titleBaseComment, m_contentBaseComment,m_id_user FROM [dbo].[BaseComment] WHERE m_id_article='{0}'", id_article), sqlConnection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        baseComment.Id = (int)reader[0];
                        baseComment.TitleBaseComment = reader[1].ToString();
                        baseComment.ContentBaseComment = reader[2].ToString();
                        userComment.Id = (int)reader[3];  
                        BaseComment pollInfo = new BaseComment();                       
                        pollInfo.Id = baseComment.Id;
                        pollInfo.TitleBaseComment = baseComment.TitleBaseComment;
                        pollInfo.ContentBaseComment = baseComment.ContentBaseComment;
                        pollInfo.User = new User();
                        pollInfo.User.Id = userComment.Id;
                        listComment.Add(pollInfo);
                    }
                }
                sqlConnection.Close();
            }
            return listComment;
        }

        public List<Article> GetListByLimit(int pageNumber, int pageSize, string sort)
        {
            var listAllArticle = new List<Article>();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {

                var sqlCmd = new SqlCommand("Articles_SelectByIndexAndSize", sqlConnection);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Page", pageNumber);
                sqlCmd.Parameters.AddWithValue("@PageSize", pageSize);
                sqlCmd.Parameters.AddWithValue("@TotalRows", 0);
                sqlCmd.Parameters.AddWithValue("@SortCol", sort);
                sqlCmd.Parameters[2].Direction = ParameterDirection.Output;

                sqlConnection.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        var dtoArticle = new ArticleDto
                        {
                            Id = (int)dr[1],
                            Title = dr[2].ToString(),
                            ContentArticle = dr[3].ToString(),
                            Nickname = dr[4].ToString()
                        };
                        listAllArticle.Add(_dtoAutoMapper.GetArticle(dtoArticle));
                    }
                }                

                sqlConnection.Close();
                var totpages = sqlCmd.Parameters[2].Value;

                return listAllArticle;
            }
        }

        public int GetCountArticles()
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var command =
                    new SqlCommand(string.Format("SELECT COUNT (*) FROM [dbo].[Article]"),
                     sqlConnection);
                int count = (int)command.ExecuteScalar();
                sqlConnection.Close();
                return count;
            }

        }

        public int GetCountCommentByArticle(int id_article)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var command =
                    new SqlCommand(string.Format("SELECT COUNT(m_id) FROM [dbo].[BaseComment] WHERE m_id_article='{0}'", id_article),
                     sqlConnection);
                int count = (int)command.ExecuteScalar();
                sqlConnection.Close();
                return count;
            }

        }

        public string GetRoleForUser(int id_user)
        {
            string role;
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT RoleName FROM [dbo].[webpages_Roles],[dbo].[webpages_UsersInRoles] WHERE [dbo].[webpages_Roles].RoleId=[dbo].[webpages_UsersInRoles].RoleId AND [dbo].[webpages_UsersInRoles].UserId={0};", id_user), sqlConnection);
                role = (string)sqlcmd.ExecuteScalar();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }

            return role;
        }

        public void UpdateRole(int id_user, string id_role)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[webpages_UsersInRoles] SET RoleId = @id_role Where UserId = @id_user",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id_user", System.Data.SqlDbType.Int)
                    .Value = id_user;
                sqlcmd.Parameters.Add("@id_role", System.Data.SqlDbType.Int)
                      .Value = id_role.ToString();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }

        }

        public void UpdateImage(int id_user, string file)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd =
                    new SqlCommand(
                        "UPDATE [dbo].[FileUpload] SET ImageName = @file Where UserId = @id_user",
                        sqlConnection);
                sqlcmd.Parameters.Add("@id_user", System.Data.SqlDbType.Int)
                    .Value = id_user;
                sqlcmd.Parameters.Add("@file", System.Data.SqlDbType.NChar)
                      .Value = file.ToString();
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                    sqlConnection.Close();
            }
        }

        public User FindByIdentityName(string first_name)
        {
            var user = new User();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                var sqlcmd = new SqlCommand(string.Format("SELECT m_id,m_firstName,m_lastName FROM [dbo].[User] WHERE m_firstName='{0}'", first_name), sqlConnection);
                using (SqlDataReader reader = sqlcmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.Id = (int)reader[0];
                        user.FirstName = reader[1].ToString();
                        user.LastName = reader[2].ToString();

                    }
                }
                sqlConnection.Close();
            }
            return user;
        }
    }
}
