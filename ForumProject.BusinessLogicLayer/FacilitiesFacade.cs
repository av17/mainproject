﻿using System;
using System.Collections.Generic;
using System.Linq;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;
using ForumProject.DataAccessLayer;
namespace ForumProject.BusinessLogicLayer
{
    public class FacilitiesFacade
    {
        private readonly IRepository<Article> _articleRep;

        private readonly IRepository<User> _userRep;

        private readonly IRepository<BaseComment> _commentRep;
   

        public FacilitiesFacade(IRepository<User> uRepository, IRepository<Article> aRepository, IRepository<BaseComment> cRepository)
        {
            _userRep = uRepository;
            _articleRep = aRepository;
            _commentRep = cRepository;
            if (!_commentRep.Generate)
            {
                Initialize();
            }
            
            if (!_articleRep.Generate)
            {
                Initialize();
            }
          
            if (!_userRep.Generate)
            {
                Initialize();
            }
        }
        
        public List<User> GetUsers()
        {
            return _userRep.GetList();
        }

        public List<Author> GetAuthors()
        {
            return _userRep.GetList().OfType<Author>().ToList();
        }

        public List<Admin> GetAdmins()
        {
            return _userRep.GetList().OfType<Admin>().ToList();
        }

        public List<Article> GetArticles()
        {
            return _articleRep.GetList();
        }

        public List<BaseComment> GetAllComments()
        {
            return _commentRep.GetList();
        }

        public List<Comment> GetComments()
        {
            return _commentRep.GetList().OfType<Comment>().ToList();
        }

        public List<Review> GetReviews()
        {
            return _commentRep.GetList().OfType<Review>().ToList();
        }

        public void DeleteUser(int id)
        {
            _userRep.Delete(id);
        }

        public void DeleteArticle(int id)
        {
            _articleRep.Delete(id);
        }
        public void DeleteComment(int id)
        {
            _commentRep.Delete(id);
        }

        public void SaveUser(User entity)
        {
            _userRep.Save(entity);
        }

        public void UpdateUser(User entity)
        {
            _userRep.Update(entity);
        }

        public void SaveArticle(Article entity)
        {
            _articleRep.Save(entity);
        }

        public void UpdateArticle(Article entity)
        {
            _articleRep.Update(entity);
        }

        public void UpdateBaseComment(BaseComment entity)
        {
            _commentRep.Update(entity);
        }

        public void SaveComment(BaseComment entity)
        {
            _commentRep.Save(entity);
        }

        public Article GetArticleById(int id)
        {
            return _articleRep.FindById(id);
        }

        public BaseComment GetCommentById(int id)
        {
            return _commentRep.FindById(id);
        }

        public User GetUserById(int id)
        {
            return _userRep.FindById(id);
        }

        private void Initialize()
        {
            var date1 = new DateTime(1999, 01, 01);
            var date2 = new DateTime(2005, 10, 10);
            var date3 = new DateTime(1800, 12, 12);
            var date4 = new DateTime(2002, 02, 14);
            var date5 = new DateTime(1700, 03, 05);
            var date6 = new DateTime(2001, 11, 11);
        
            var authors = new List<Author>();
            authors.Add(new Author(1) { FirstName = "Author1",LastName="Ivanov",Birthday=date1,NickName="iva",Popularity=1.25 });
            authors.Add(new Author(2) { FirstName = "Author2", LastName = "Petrov", Birthday = date2, NickName = "pet", Popularity = 5.0 });
            authors.Add(new Author(3) { FirstName = "Author3", LastName = "Pittov", Birthday = date3, NickName = "pit", Popularity = 8.95 });
            foreach (var author in authors)
            {
                _userRep.Save(author);
            }

            var users = new List<User>
            {
                new User(1) {FirstName = "User1", LastName = "Ivanov", Birthday = date1},
                new User(2) {FirstName = "User2", LastName = "Petrov", Birthday = date2},
                new User(3) {FirstName = "User3", LastName = "Sidorov", Birthday = date3},
                new User(4) {FirstName = "User4", LastName = "Ogurcov", Birthday = date4},
                new User(5) {FirstName = "User5", LastName = "Kononov", Birthday = date5},
                new User(6) {FirstName = "User6", LastName = "Ahatov", Birthday = date6}
            };
            foreach (var user in users)
            {
                _userRep.Save(user);
            }

            var admins = new List<Admin>
            {
                new Admin(1) {FirstName = "Admin1", LastName = "Ivanov", Birthday = date1},
                new Admin(2) {FirstName = "Admin2", LastName = "Petrov", Birthday = date2},
                new Admin(3) {FirstName = "Admin3", LastName = "Pittov", Birthday = date3}
            };
            foreach (var admin in admins)
            {
                _userRep.Save(admin);
            }

            var articles = new List<Article>
            {
                new Article(1) {Title = "title1", ContentArticle = "content1", Author = authors[0]},
                new Article(2) {Title = "title2", ContentArticle = "content2", Author = authors[1]},
                new Article(3) {Title = "title3", ContentArticle = "content3", Author = authors[2]}
            };
            foreach (var article in articles)
            {
                _articleRep.Save(article);
            }

            _commentRep.Save(new Comment(1, "comment1", users[0], articles[0]));
            _commentRep.Save(new Comment(2, "comment2", users[1], articles[1]));
            _commentRep.Save(new Comment(3, "comment3", users[1], articles[1]));
            _commentRep.Save(new Review(1, "review1", users[0], articles[0], 4));
            _commentRep.Save(new Review(2, "review2", users[2], articles[2], 4));
                  
        }

        public List<string> GetUserByBirthday(DateTime comparedDate)
        {
              
            List<User> users =_userRep.GetList();
            List<string> newListUsers=new List<string>();
            foreach (var user in users)
            {
                if (user.Birthday.Year <= comparedDate.Year)
                {
                    newListUsers.Add(user.FirstName);
                }

            }
            return newListUsers;
          
        }

        public string GetCurrentUser(List<Author> _author)
        {
            int _idCurrentUser = _author[0].Id;
            string _currentUser = _author[0].NickName;
            return _currentUser;

        }
        
        public List<BaseComment> GetBaseCommentByArticle(int id_article)
        {
            DBMethods methods = new DBMethods();
            var ListBaseComments= methods.GetBaseCommentByArticle(id_article);
            return ListBaseComments;
        }

        public User FindByIdentityName(string first_name)
        {
            DBMethods methods = new DBMethods();
            User user = methods.FindByIdentityName(first_name);
            return user;
        }

        public void UpdateRole(int id_user, string id_role)
        {
            DBMethods methods = new DBMethods();
            methods.UpdateRole(id_user, id_role);       
        }

        public void UpdateImage(int id_user, string file)
        {
            DBMethods methods = new DBMethods();
            methods.UpdateImage(id_user, file);
        }

        public List<Article> GetListByLimit(int pageNumber, int pageSize,string sort)
        {
            DBMethods methods = new DBMethods();
            var ListArticles = methods.GetListByLimit(pageNumber, pageSize,sort);
            return ListArticles;
        }

        public int GetCountCommentByArticle(int id_article)
        {
            DBMethods methods = new DBMethods();
            var CountCommentByArticle = methods.GetCountCommentByArticle(id_article);
            return CountCommentByArticle;
        }

        public int GetCountArticles()
        {
            DBMethods methods = new DBMethods();
            var CountArticles = methods.GetCountArticles();
            return CountArticles;
        }

        public string GetRoleForUser(int id_user)
        {
            DBMethods methods = new DBMethods();
            var RoleForUser = methods.GetRoleForUser(id_user);
            return RoleForUser;
        }

        public List<string> GetUserByComment()
        {

            List<BaseComment> comments =_commentRep.GetList();
            List<string> newListUsers = new List<string>();
            Dictionary<int, string> newDic = new Dictionary<int, string>();
            foreach (var comment in comments)
            {              
                newDic.Add(comment.Id, comment.User.FirstName);
            }

            foreach (var a in newDic)
            {
                newListUsers.Add(a.Value); 
            }

            Dictionary<string, int> count = new Dictionary<string, int>();
            for (int i = 0; i < newListUsers.Count; i++)
            {
                for (int j = i; j < newListUsers.Count; j++)
                {
                    if (newListUsers[i] != newListUsers[j])
                    {
                        count.Add(newListUsers[i], j - i);
                        i = j - 1;
                        break;
                    }
                    if (j == newListUsers.Count - 1)
                    {
                        count.Add(newListUsers[i], j - i + 1);
                        i = j;
                    }
                }
            }

            List<string> userComment2 = new List<string>();
            foreach (var a in count)
            {
                if (a.Value >= 2)
                {
                    userComment2.Add(a.Key);
                }
            }
            return userComment2;
        }

        public int GetCountCommentByRaiting() 
        {
            List<Review> review = _commentRep.GetList().OfType<Review>().ToList();
            List<string> listComment = new List<string>();
            foreach (var user in review)
            {
                int rating = user.SetRating;
                if (rating >= 3)
                {
                    listComment.Add(user.TitleBaseComment);
                }
            }
            return listComment.Count;

        }
       
        public float GetAverageRating(int idArticle, List<Review> listOfReviews)
        {
            int numberOfArticles = 0;
            int totalRating = 0;
            foreach (Review currentReview in listOfReviews)
            {
                if (currentReview.Article.Id == idArticle)
                {
                    numberOfArticles += 1;
                    totalRating += currentReview.SetRating;
                }
                else
                {
                }
            }
            if (numberOfArticles == 0)
            {
                return -1;
            }
            else
            {
                return (float)totalRating / numberOfArticles;
            }
        }

        public User AddUser(int id, string name, string lastName, DateTime birthday)
        {
            User user = new User(id, name, lastName, birthday);
            _userRep.Save(user);
            return user;
        }

        public Article AddArticle(int id, Author author, string title, string content)
        {
            var article = new Article(id) { Author = author, Title = title, ContentArticle = content };
            _articleRep.Save(article);
            return article;
        }

        public Comment AddComment(int id, Article article, User user, string content)
        {
            Comment comment = new Comment(id, content, user, article);
            _commentRep.Save(comment);
            return comment;
        }

        public Review AddReview(int id, string content,  User user, Article article,int rating)
        {
            var review = new Review(id, content, user, article, rating);           
            _commentRep.Save(review);
            return review;
        }

    }
}
