﻿using System.Collections.Generic;
using ForumProject.DataAccessLayer.Repository;
using ForumProject.Entity;

namespace ForumProject.Test.UnitTests
{
    public class ArticleMock : IRepository<Article>
    {        
        private Article _article;
        private readonly List<Article> _contextArticle;

        public bool Generate
        {
            get {
                return _contextArticle.Count != 0;
            }
        }
        public ArticleMock(List<Article> articles)
        {
            _contextArticle = articles;
        }
        public Article FindById(int id)
        {
            foreach (var item in _contextArticle)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
       
        public List<Article> GetList()
        {
            return _contextArticle;
        }

        public void Save(Article entity)
        {
            _contextArticle.Add(entity);
            _article = entity;
        }        

        public void Delete(int id)
        {
            var entity = FindById(id);
            _contextArticle.Remove(entity);
            _article = entity;
        }

        public void Update(Article entity)
        {
          
        }
       
    }
}
