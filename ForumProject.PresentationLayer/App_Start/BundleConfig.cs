﻿using System.Web;
using System.Web.Optimization;

namespace ForumProject.PresentationLayer.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                         "~/Scripts/jquery-{version}.min.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js",
                       "~/Scripts/jquery-ui-{version}.min.js",
                         "~/Scripts/jquery.ui*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                       "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/cookie").Include(
                         "~/Scripts/jquery_cookie.js"));

            bundles.Add(new ScriptBundle("~/bundles/microsoftajax").Include(
                         "~/Scripts/MicrosoftAjax*",
                          "~/Scripts/MicrosoftMvcAjax*",
                           "~/Scripts/MicrosoftMvcValidation*"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                               "~/Scripts/DatePickerReady.js"));

            bundles.Add(new ScriptBundle("~/bundles/myScripts").Include(
                                "~/Scripts/myScripts.js"));

            bundles.Add(new ScriptBundle("~/bundles/ListGridView").Include(
                                  "~/Scripts/ListGridView.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                            "~/Scripts/angular.js",
                            "~/Scripts/angular-sanitize.js",
                             "~/Scripts/angular-route.js",
                             "~/Scripts/Angular/home.controller.js",
                              "~/Scripts/Angular/edit.controller.js",
                              "~/Scripts/Angular/list.controller.js",
                              "~/Scripts/Angular/main.module.js",
                               "~/Scripts/Angular/users.service.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularsingle").Include(
                            "~/Scripts/angular.js",
                            "~/Scripts/angular-sanitize.js",
                             "~/Scripts/angular-route.js",
                                "~/Scripts/Angular/SinglePage/home.js",
                                "~/Scripts/Angular/SinglePage/module.js",
                                 "~/Scripts/Angular/SinglePage/singlePage1.js",
                                  "~/Scripts/Angular/SinglePage/singlePage2.js",
                                   "~/Scripts/Angular/SinglePage/singlePage3.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/PagedList.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css",
                        "~/Content/themes/base/style.css"));
        }
    }
}

