﻿using System;
using ForumProject.Entity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace ForumProject.PresentationLayer.Models
{
    public class ArticleViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required!")]
        [Display(Name = "Title")]      
        public String Title { get; set; }

        [Required(ErrorMessage = "Content is required!")]    
        [Display(Name = "Content")]  
        public String ContentArticle { get; set; }        
        public String NickName { get; set; }

        public List<CommentViewModel> BaseComments { get; set; }
        public int CountCommentByArticle { get; set; }

        public PageInfo PageInfo { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; }
        
        public string PageTitle { get; set; }
        public IEnumerable<Article> ArticleList { get; set; }
        public int ArticleCount { get; set; }

        
    }
}