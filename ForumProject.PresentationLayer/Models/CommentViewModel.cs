﻿using System;
using ForumProject.Entity;
using System.ComponentModel.DataAnnotations;

namespace ForumProject.PresentationLayer.Models
{
    public class CommentViewModel 
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Comment title is required!")]
        public string TitleBaseComment { get; set; }

        [Required(ErrorMessage = "Content is required!")]
        public string ContentBaseComment { get; set; }

        public User User { get; set; }
        public Article Article { get; set; }

        public CommentViewModel()
        {
            this.User = new User();
        }
        public int IdUser
        {
            get { return User.Id; }
            set { User.Id = value; }
        }
        public string FirstNameUser
        {
            get { return User.FirstName; }
            set { User.FirstName = value; }
        }
        public string LastNameUser
        {
            get { return User.LastName; }
            set { User.LastName = value; }
        }

    }
}
