﻿using System;
using ForumProject.Entity;
using Omu.ValueInjecter;

namespace ForumProject.PresentationLayer.Models
{
    public static class ArticleMapper
    {
        public static Func<Article, ArticleViewModel> ToViewModel = article =>
        {
            var viewModel = new ArticleViewModel();
            viewModel.NickName = article.Author.NickName;
            viewModel.InjectFrom(article);

            return viewModel;
        };

        public static Article ToModel(this ArticleViewModel viewModel)
        {
            
            var model = new Article();
            model.Author = new Author(viewModel.NickName);
            model.InjectFrom(viewModel);

            return model;
        }

        public static ArticleViewModel ArticleViewModel(this Article model)
        {
            var viewModel = new ArticleViewModel();
            viewModel.InjectFrom(model);

            return viewModel;
        }
    }
}

