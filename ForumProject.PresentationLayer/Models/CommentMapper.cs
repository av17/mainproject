﻿using System.Web.Security;
using ForumProject.Entity;
using Omu.ValueInjecter;
using System;

namespace ForumProject.PresentationLayer.Models
{
    public static class CommentMapper
    {
        public static Func<BaseComment, CommentViewModel> ToViewModel = comment =>
        {
            var viewModel = new CommentViewModel();
         
            viewModel.InjectFrom(comment);

            return viewModel;
        };

        public static BaseComment ToModel(this CommentViewModel viewModel)
        {
            var model = new BaseComment();
           
            model.InjectFrom(viewModel);

            return model;
        }

        public static CommentViewModel CommentViewModel(this BaseComment model)
        {
            var viewModel = new CommentViewModel();
            viewModel.InjectFrom(model);

            return viewModel;
        }       
    }
}
