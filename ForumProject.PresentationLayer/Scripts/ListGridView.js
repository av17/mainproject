﻿///List View, Grid View- Cookie
$(function () {

    var view = $.cookie('list_grid');
    if (view == 'grid') {
        $('#container ul').addClass('grid');
    }
    else {
        $('#container ul').addClass('list');
    }

});


$('button').on('click', function (e) {
    if ($(this).hasClass('grid')) {
        $('#container ul').removeClass('list').addClass('grid');

        $.cookie('list_grid', 'grid');
    }
    else if ($(this).hasClass('list')) {
        $('#container ul').removeClass('grid').addClass('list');
        $.cookie('list_grid', null);
    }
});
