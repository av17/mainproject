﻿using ForumProject.PresentationLayer.Models;
using log4net;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace ForumProject.PresentationLayer.Controllers
{    
    public class AccountController : Controller
    {
        private readonly ILog m_logger = LogManager.GetLogger(typeof(AccountController));
    
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            m_logger.Info("Login page");   
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    m_logger.Info("Login Successful");  
                    return RedirectToAction("Index", "Article");
                }
            }
            ModelState.AddModelError("", Resources.Resource.IncorrectlyPasswordLogin);
            m_logger.Error("Incorrectly Password or Login");
            return View(model);           
        }
        
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    m_logger.Info("Begin Register");                    
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password,
                        new { m_firstName = model.UserName, m_lastName = model.LastName, m_birthday = model.Birthday });
                    WebSecurity.Login(model.UserName, model.Password);
                    Roles.AddUserToRole(model.UserName, "User");
                    m_logger.Info("Register Successful"); 
                    return RedirectToAction("Index", "Article");
                }

                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", "User already exists");
                    m_logger.Error("Register Method");
                    m_logger.ErrorFormat("First Name={0},Last Name={1},Birthday={2}, Password={3}", model.UserName, model.LastName, model.Birthday, model.Password);
                    m_logger.Error("Error when registering a new user", e);                  
                }
            }
            return View(model);
        }
        
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            m_logger.Info("Login page");   
            return RedirectToAction("Login", "Account");
        }        
    }
}