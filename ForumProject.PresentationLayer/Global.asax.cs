﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using ForumProject.Entity;
using WebMatrix.WebData;
using ForumProject.PresentationLayer.App_Start;
using System.Web.Optimization;
namespace ForumProject.PresentationLayer
{    
    public class MvcApplication : System.Web.HttpApplication
    {       
        protected void Application_Start()
        {
            Logger.InitLogger();     
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }       
    }
}