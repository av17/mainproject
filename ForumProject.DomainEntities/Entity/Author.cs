﻿namespace ForumProject.Entity
{
   public class Author:User
    {
        public string NickName { get; set; }
        public double Popularity { get; set; }  
        public Author(int id): base(id)
        {
        }
        public Author(string nickname)
        {
            NickName = nickname;
        }
        public Author()
        {
           
        }
    }
}
